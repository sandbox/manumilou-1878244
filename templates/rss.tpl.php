<?php

/**
 *  @file
 *  Generic RSS container template file used to render rss feed
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */

global $base_url;

?>


<rss version="2.0" xml:base="<?php echo $base_url . '/' . $feed_url; ?>" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <channel>
    <title>Multiple RSS Feed Sample</title>
		<link><?php echo $base_url . '/' . $feed_url; ?></link>
		<description><?php echo $feed_description; ?></description>
		<language>en</language>
    <?php echo $page; ?>
  </channel>
</rss>
