<?php

/**
 *  @file
 *  Generic RSS template file used to render every rss feed
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */

?>

<title><?php print $node_title; ?></title>
<link><?php print $node_link; ?></link>
<pubDate><?php print $node_created; ?></pubDate>
<guid isPermaLink="true"><?php print $node_id; ?></guid>
<!-- Creator -->
<dc:creator><?php print $node_author; ?></dc:creator>
<!-- Description -->
<description>
<![CDATA[
<?php foreach($node_fields as $node_field): ?>
	<?php print $node_field; ?>
<?php endforeach; ?>
]]>
</description>


