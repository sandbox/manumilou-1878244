<?php

/**
 *  @file
 *  Handle feeds configuration. Every RSS/MRSS feeds must be declared here to be automatically generated.
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */

return $feeds = array(
  'sample' => array(
		'sample1' => array(
			'url' => 'rss/sample1.xml',
			'title' => 'Multiple RSS feeds - Sample 1',
			'html' => TRUE,
			'entityCondition' => array(
				'bundle' => 'page', /* TODO Change this */
			),
			'propertyCondition' => array(		
				'type' => 'page', /* TODO Change this */
				'status' => '1',
			),
			'fieldCondition' => array(
				/* TODO Add field conditions here */
			),
			'fields' => array(
				array('value' => 'body', 'func' => 'teaser'),
				/* TODO Add more fields here */
			),
			'fieldOrderBy' => array(
					
			),
		),
  ), 
  /* ADD MORE FEED DESCRIPTIONS HERE */
);
