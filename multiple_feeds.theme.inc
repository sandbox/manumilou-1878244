<?php

/**
 *  @file
 *  Templates preprocessing functions for RSS TabTimes feeds
 *
 *  @copyright 2012 Savoir-faire Linux, inc.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @author Emmanuel Milou <emmanuel.milou@savoirfairelinux.com>
 *
 */


/**
 * Preprocesses the variables for items.tpl.php.
 *
 * @see items.tpl.php
 */
function template_preprocess_items(&$variables) {
	$feed = $variables['feedConfig'];
  foreach ($variables['items'] as $nid => $content) {
    $output = theme('rss_content', array('content' => $content, 'fields' => $feed->fields));
    $variables['items'][$nid] = $output;
  }
}

/**
 * Preprocesses the variables for rss-content.tpl.php.
 *
 * @see rss-content.tpl.php
 */
function template_preprocess_rss_content( &$variables ) {
	
	global $base_url;
	
  $content = $variables['content'];
	$fields = $variables['fields'];
	
	// Set mandatory fields
	$variables['node_title'] = $content->title;
	$variables['node_link'] = $base_url . '/' . drupal_get_path_alias('node/' . $content->nid);
	$variables['node_created'] = $content->created;
	$variables['node_id'] = $base_url . '/node/' . $content->nid;
	
	// Get all fields specified in configuration
	$variables['node_fields'] = array();
	foreach($fields as $field) {
		if(is_array($field)) {
			$variables['node_fields'][$field['value']] = call_user_func($field['func'], @$content->{$field['value']}['und'][0]['value']);
		} else {
			$variables['node_fields'][$field] = @$content->{$field}['und'][0]['value'];
  	}
	}
}

function teaser($value) {
	return $value;
}